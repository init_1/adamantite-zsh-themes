# ADAMANTITE ZSH THEMES
![preview](adamantite.png)
#### INSTALL INSTRUCTION
Open your terminal and be secure to have [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh) installed, otherwise install it.
> ##### GET IT 
>````bash 
>curl -L "https://gitlab.com/init_1/adamantite-zsh-themes/raw/master/adamantite.zsh-theme" > ~/.oh-my-zsh/themes/adamantite.zsh-theme 
>````
> ##### CHANGE YOUR ZSH THEMES
> Open your `.zshrc` with your prefer file editor and change `ZSH_THEME` variable with "`adamantite`":
> ````zsh 
> ZSH_THEME=adamantite
> ````
#### PRO TIPS
You can customize the behavior of the some part of the theme using some environment variables in accord with the [zsh prompt-expansion](http://zsh.sourceforge.net/Doc/Release/Prompt-Expansion.html). 
For now the following variables are supported:
> ##### FOR CHANGE THE 'USER@MACHINE' SEGMENT
> ````zsh
> ADAMANTITE_CONTEXT=%n@%m
> ````
> where:
> * `%n`: print the username
> * `%m`: print the machine name
> ##### FOR CHANGE THE FOLDER SETTING
> the default behavior of the folder segment shows only the last 2 folders you are in, for show the entire position:
> ````zsh
> ADAMANTITE_PATH=%~
> ````
> Put before '~' the number of how many folder at lest zsh can print (for example the default value is `'%2~'`)

Maybe in the future I make more variables for change the theme behavior, for now be free to mod and redistribute my theme 
my theme as you see fit... So for now... __HAPY HACHING__ :)